<?php
	require_once(dirname(__FILE__).'/util/DB.php');
	class User{
		private $cachedInfo;
		public function __construct($id, $lock = true)
		{
			global $DB;
			$query = "Select `Id`, `ServerId`, `UserName`, `LastAction`, `NextAction`, CURRENT_TIMESTAMP(6) as now, `CurrentGold`, `CurrentCity`, `CurrentCapacity` from `users` where `id` = $id";
			$res = $DB->query($query);
			if ($res)
			{
				$row = $res->fetch_assoc();
				$now = $row["now"];
				$this->cachedInfo = array(
					"id" => $row["Id"],
					"username" => $row["UserName"],
					"server" => $row["ServerId"],
					"lastAction" => $row["LastAction"],
					"nextAction" => $row["NextAction"],
					"gold" => $row["CurrentGold"],
					"city" => $row["CurrentCity"],
					"capacity" => $row["CurrentCapacity"]);
			}
			else
				printDBError($query);
			if ($lock && $this->city >= 0)
			{
				if ($this->cachedInfo["nextAction"] == null || $this->cachedInfo["nextAction"] > $now)
					die('one request at a time!');
				$query = "UPDATE `users` SET `NextAction` = null, `LastAction` = CURRENT_TIMESTAMP(6), `nextAction` = CURRENT_TIMESTAMP(6) + 1 WHERE `id` = $id";
				$res = $DB->query($query);
				if (!$res)
					printDBError($query);
			}
		}
		
		public function __get($name)
		{
			if (isset ($this->cachedInfo[$name]))
				return $this->cachedInfo[$name];
			else
			{
				$this->cache($name);
				if (!isset ($this->cachedInfo[$name]))
				{
					echo "requested bad attribute from user: $name";
					die;
				}
				return $this->cachedInfo[$name];
			}
		}
		
		public function cache($name)
		{
			global $DB;
			if ($name == "goldCostForUpgrade" || $name == "oracleCost")
			{
				$query = "SELECT `IncreaseCapacityCost`, `OracleCost` FROM `servers` WHERE `id` = $this->server";
				
				$res = $DB->query($query);
				if ($res)
				{
					$row = $res->fetch_row();
					$this->cachedInfo["goldCostForUpgrade"] = $row[0] * $this->capacity * $this->capacity;
					$this->cachedInfo["oracleCost"] = $row[1] * $this->gold;
				}
				else
					printDBError($query);
			}
		}
		
		public function setCooldown($seconds)
		{
			global $DB;
			$query = "UPDATE `users` SET `NextAction` = `LastAction` + $seconds where `Id` = $this->id";
			if (!($res = $DB->query($query)))
				printDBError($query);
		}
	};
	