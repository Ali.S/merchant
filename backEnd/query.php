<?php
	unset ($query);
	$shouldLog = true;
	switch ($command[1])
	{
		case "towns": 
			$cooldown = 0;
			$query = "SELECT `Id`, `PosX`, `PosY` FROM `cities` WHERE `ServerId` = $user->server"; 
			break;
		case "economy" : 
			$cooldown = 0;
			$query = "SELECT `CityId`, `GoodId`, `BasePriceBuy`, `BasePriceSell`, `Production`, `Consumption`, `MaxStore`, `StockInfluenceOnPrice`
				FROM goodsincities join(cities) ON cities.`Id` = `CityId` WHERE `ServerId` = $user->server"; 
				break;
		case "market": 
			$cooldown = 0.25;
			$query = "SELECT `GoodId`, `Stock`, `BasePriceBuy` * pow(1-`StockInfluenceOnPrice`, `Stock`) AS buyPrice, 
				`BasePriceSell` * pow(1-`StockInfluenceOnPrice`, `Stock`) AS sellPrice FROM goodsincities WHERE `CityId` = $user->city"; 
				break;
		case "me":
			$cooldown = 0;
			echo "$user->id $user->gold $user->city $user->capacity $user->goldCostForUpgrade $user->oracleCost\n";
			$query = "SELECT `GoodId`, `Amount` FROM cargo WHERE `UserId` = $user->id";
			break;
		case "players":
			if ($user->city == -1)
			{
				$cooldown = 0;
				$query = "SELECT `Id`, `UserName` ,`CurrentCity`, `CurrentGold`, `HighestScore`  FROM `users` WHERE `ServerId` = $user->server and `CurrentCity` >= 0 order by `HighestScore` DESC";
			}
			else
			{
				$cooldown = 0.5;
				$query = "SELECT `Id`, `CurrentGold` FROM `users` WHERE `CurrentCity` = $user->city";
			}
			break;
		case "oracle":
			if (!isset ($command[2]) || !ctype_digit($command[2]))
			{
				echo "invalid city id.";
				break;
			}
			
			$target = intval($command[2]);
			$query = "SELECT `ServerId` from `cities` where `id` = $target";
			if (!($res = $DB->query($query)))
				printDBError($query);
			if ($res->num_rows == 0 || $res->fetch_row()[0] != $user->server)
			{
				echo "invalid city id";
				break;
			}			
			$query = "UPDATE `users` set `CurrentGold` = `CurrentGold` - $user->oracleCost where `Id` = $user->id";
			if (!($res = $DB->query($query)))
				printDBError($query);
			$query = "INSERT INTO `gamelog` (`UserId`, `TargetCity`, `command`) values ($user->id, $target, 'query oracle')";
			if (!$DB->query($query))
				printDBError();
			
			$query = "SELECT `GoodId`, `Stock`, `BasePriceBuy` * pow(1-`StockInfluenceOnPrice`, `Stock`) AS buyPrice, 
				`BasePriceSell` * pow(1-`StockInfluenceOnPrice`, `Stock`) AS sellPrice FROM goodsincities WHERE `CityId` = $target"; 
			$cooldown = 1;
			
			$shouldLog = false;
			break;
		default:
			echo "bad request\n";
			$shouldLog = false;
			break;
	}
	
	if (isset ($query))
	{
		$res = $DB->query($query);
		if ($res)
		{
			echo $res->num_rows."\n";
			while ($row = $res->fetch_row())
			{
				echo join(' ', $row)."\n";
			}
		}
		else
		{
			printDBError($query);
		}
	}
	
	if ($shouldLog)
	{
		$query = "INSERT INTO `gamelog` (`UserId`, `command`) values ($user->id, 'query $command[1]')";
		if (!$DB->query($query))
			printDBError();
	}