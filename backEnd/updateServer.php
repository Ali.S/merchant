<?php
	require_once (dirname(__FILE__)."/util/DB.php");
	require_once (dirname(__FILE__)."/util/misc.php");
	$serverId = intval($argv[1]);
	$query = "SELECT `timeScale` from `servers` where `Id` = $serverId";
	if (!($res = $DB->query($query)))
		printDBError($query);
	else
		$time = 1 / $res->fetch_row()[0];
	while(true)
	{
		$query = "UPDATE `goodsincities` 
					JOIN `goods` On `goods`.`Id` = `goodsincities`.`GoodId` 
					JOIN `cities` On `cities`.`Id` = `goodsincities`.`CityId`   
					set `Stock` = GREATEST(LEAST(`Stock` + Production - Consumption, `MaxStore`), 0) 
					WHERE `cities`.`ServerId` = $serverId and `goods`.`ServerId` = $serverId";
		if (!($res = $DB->query($query)))
			printDBError($query);
		echo "updated.\n";
		usleep($time * 1000000);
	}
?>