<?php
	if (isset ($_POST["user"]) && isset ($_POST["pass"]))
	{
		require_once (dirname(__FILE__)."/util/DB.php");
		require_once (dirname(__FILE__)."/util/misc.php");
		if (isset ($_POST["register"]) && $_POST["register"] == true)
		{
			$salt = generateRandomString();
			$user = $DB->escape_string(trim($_POST["user"]));
			$pass = $DB->escape_string($_POST["pass"]);
			if (isset($_POST["server"]))
			{
				$server = intval($_POST["server"]);
				$query = "SELECT `InitialGold`, `InitialCapacity`, `InitialCity` from servers where `Id` = $server";
				if ($res = $DB->query($query))
				{
					$row = $res->fetch_assoc();
					$InitialGold = $row['InitialGold'];
					$InitialCapacity = $row['InitialCapacity'];
					$InitialCity = $row['InitialCity'];
					if ($InitialCity == -1)
					{
						$query = "SELECT `Id` from cities where `ServerId` = $server order by RAND() limit 1";
						if ($res = $DB->query($query))
						{
							$InitialCity = $res->fetch_assoc()["Id"];
						}
						else
							printDBError($query);
					}
				}
				else
					printDBError($query);
				$query = "INSERT into users ".
					"(`UserName`, `Password`, `Salt`,`ServerId`, `CurrentGold`, `HighestScore`, `CurrentCity`, `CurrentCapacity`)".
					"VALUES ('$user',sha2('$pass$salt',256),'$salt', $server, $InitialGold, $InitialGold, $InitialCity, $InitialCapacity)";
			}
			else
			{
				$query = "INSERT into users ".
					"(`UserName`, `Password`, `Salt`,`ServerId`, `CurrentGold`, `HighestScore`, `CurrentCity`, `CurrentCapacity`)".
					"VALUES ('$user',sha2('$pass$salt',256),'$salt', -1, 0, 0, 0, 0)";
			}
			if ($DB->query($query))
			{
				$_SESSION["uid"] = $DB->insert_id;
			}
			else
				printDBError($query);
		}
		else
		{
			$user = $DB->escape_string(trim($_POST["user"]));
			$pass = $DB->escape_string($_POST["pass"]);
			$query = "SELECT `id` from `users` where `UserName` like '$user' and `password` like sha2(concat('$pass', (select `salt` from `users` where `username` like '$user')), 256)";
			$result = $DB->query($query);
			if ($result)
			{
				if ($result->num_rows > 0)
				{
					$_SESSION["uid"] = $result->fetch_row()[0];
					header("X-Login-Result: succeed");
				}
				else
				{
					header("X-Login-Result: failed");
				}
			}
			else
				printDBError($query);
		}
	}
