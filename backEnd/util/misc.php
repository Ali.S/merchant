<?php
	function printDBError($query)
	{
		global $DB;
		echo "Error executing query: \n\n$query\n\nError:$DB->error";
		debug_print_backtrace();
		die;
	}
	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function gamma($mean) {
		
		
		// This is a translation of Python Software Foundation licensed code from the Python project.
		/*$alpha = sqrt($mean);
		$beta = sqrt($mean);
		if ($mean < 1)*/
		{
			$alpha = 10;
			$beta = $mean / 10;
		}

		if($alpha > 1) {
			// Uses R.C.H. Cheng, "The generation of Gamma variables with non-integral shape parameters", Applied Statistics, (1977), 26, No. 1, p71-74
			$ainv = sqrt(2.0 * $alpha - 1.0);
			$bbb = $alpha - log(4.0);
			$ccc = $alpha + $ainv;

			while (true) {
				$u1 = rand() / getrandmax();

				if (!((1e-7 < $u1) && ($u1 < 0.9999999))) {
					continue;
				}

				$u2 = 1.0 - (rand()/getrandmax());
				$v = log($u1 / (1.0-$u1))/$ainv;
				$x = $alpha * exp($v);
				$z = $u1 * $u1 * $u2;
				$r = $bbb+$ccc*$v-$x;
				$SG_MAGICCONST = 1 + log(4.5);
				if ($r + $SG_MAGICCONST - 4.5*$z >= 0.0 || $r >= log($z)) {
					return $x * $beta;
				}
			}
		} else if ($alpha == 1.0) {
			$u = rand()/getrandmax();
			while ($u <= 1e-7) {
				$u = rand()/getrandmax();
			}
			return -log($u) * $beta;
		} else { // 0 < alpha < 1
			// Uses ALGORITHM GS of Statistical Computing - Kennedy & Gentle
			while (true) {
				$u3 = rand()/getrandmax();
				$b = (M_E + $alpha)/M_E;
				$p = $b*$u3;
				if ($p <= 1.0) {
					$x = pow($p, (1.0/$alpha));
				}
				else {
					$x = log(($b-$p)/$alpha);
				}
				$u4 = rand()/getrandmax();
				if ($p > 1.0) {
					if ($u4 <= pow($x, ($alpha - 1.0))) {
						break;
					}
				}
				else if ($u4 <= exp(-$x)) {
					break;
				}
			}
			return $x * $beta;
		}
	}