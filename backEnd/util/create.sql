# should run with delemiter set as '//'

CREATE TABLE IF NOT EXISTS `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ServerId` int(11) NOT NULL,
  `UserName` varchar(20) NOT NULL,
  `Password` varchar(80) NOT NULL,
  `Salt` varchar(20) NOT NULL,
  `Created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `LastAction` timestamp(6) NULL DEFAULT NULL,
  `NextAction` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6),
  `CurrentGold` float NOT NULL,
  `HighestScore` float NOT NULL,
  `CurrentCity` int(11) NOT NULL,
  `CurrentCapacity` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UserName` (`ServerId`, `UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `cities` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ServerId` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `PosX` float NOT NULL,
  `PosY` float NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`ServerId`, `Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `goods` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ServerId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`ServerId`, `Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `goodsincities` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CityId` int(11) NOT NULL,
  `GoodId` int(11) NOT NULL,
  `BasePriceBuy` float NOT NULL,
  `BasePriceSell` float NOT NULL,
  `Production` int(11) NOT NULL,
  `Consumption` int(11) NOT NULL,
  `Stock` int(11) NOT NULL,
  `MaxStore` int(11) NOT NULL,
  `StockInfluenceOnPrice` float NOT NULL,
  PRIMARY KEY(`Id`),
  UNIQUE KEY `Id` (`CityId`,`GoodId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `servers` (
	`Id` int(11) NOT NULL AUTO_INCREMENT,
	`Name` varchar(20) NOT NULL,
	`TimeScale` float NOT NULL,
	`InitialGold` float NOT NULL,
	`InitialCapacity` int(11) NOT NULL,
	`IncreaseCapacityCost` float NOT NULL,
	`OracleCost` float NOT NULL,
	`InitialCity` int(11) NOT NULL,
	PRIMARY KEY (`Id`),
	UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `gamelog` (
	`Id` int(11) NOT NULL AUTO_INCREMENT,
	`UserId` int(11) NOT NULL,
	`ActionTime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
	`goodincityId` int(11) NULL,
	`Amount` int(11) NULL,
	`TargetCity` int(11) NULL,
	`Command` varchar(20) NOT NULL,
	PRIMARY KEY(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `cargo` (
	`Id` int(11) NOT NULL AUTO_INCREMENT,
	`UserId` int(11) NOT NULL,
	`GoodId` int(11) NOT NULL,
	`Amount` int(11) NOT NULL,
	PRIMARY KEY(`Id`),
	UNIQUE KEY `Cargo` (`UserId`, `GoodId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE TABLE IF NOT EXISTS `submissions` (
	`Id` int(11) NOT NULL AUTO_INCREMENT,
	`Lang` varchar(5) NOT NULL,
	`UserId` int(11) NOT NULL,
	`FileId` varchar(28) NOT NULL,
	`Verified` Boolean NOT NULL DEFAULT false,
	PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin//

CREATE FUNCTION buyProduct(userid int, goodid int, amount int) RETURNS int
  BEGIN
    DECLARE cityid int;
	DECLARE goodincityid int;
	DECLARE gold int;
    DECLARE stock int;
	DECLARE capacity int;
	DECLARE usedCapacity int;
	DECLARE price float;
	
	SELECT `users`.`currentCapacity`, `users`.`CurrentCity`, `users`.`CurrentGold` into capacity, cityid, gold from `users` where `users`.`id` = userid;
	SELECT `goodsincities`.`stock`, `BasePriceBuy` * pow(1-`StockInfluenceOnPrice`, `goodsincities`.`stock`) * amount, id into stock, price, goodincityid
		from `goodsincities` where `goodsincities`.`CityId` = cityid AND `goodsincities`.`GoodId` = goodid;
	
	SELECT sum(`cargo`.`Amount`) into usedCapacity from `cargo` where `cargo`.`UserId` = userid;
	if (usedCapacity is null) then
		set usedCapacity = 0;
	end if;
	
	if amount + usedCapacity > capacity then
		return -1;
	end if;
	if stock < amount then
		return -2;
	end if;
	if price > gold then
		return -3;
	end if;
	update `goodsincities` set `goodsincities`.`stock` = `goodsincities`.`stock` - amount where `goodsincities`.`CityId` = cityid AND `goodsincities`.`GoodId` = goodid;
	update `users` set `users`.`CurrentGold` = `users`.`CurrentGold` - price, `LastAction` = CURRENT_TIMESTAMP(6) where `id` = userid;
	insert into `cargo` (`userid`, `GoodId`, `Amount`) values (userid, goodid, amount) ON DUPLICATE KEY UPDATE `cargo`.`Amount` = `cargo`.`Amount` + amount;
	insert into `gamelog` (`UserId`, `goodincityId`, `Amount`, `command`) values (userid, goodincityid, amount, 'buy');
	return LAST_INSERT_ID();
  END //
  
CREATE FUNCTION sellProduct(userid int, goodid int, amount int) RETURNS int
  BEGIN
    DECLARE cityid int;
	DECLARE goodincityid int;
    DECLARE citystock int;
	DECLARE maxcitystock int;
	DECLARE price float;
	DECLARE userstock int;
	set userstock = 0;
	SELECT `users`.`CurrentCity` into cityid from `users` where `users`.`id` = userid;
	SELECT `goodsincities`.`stock`, `goodsincities`.`MaxStore`, `BasePriceSell` * pow(1-`StockInfluenceOnPrice`, `goodsincities`.`stock`) * amount, id 
		into citystock, maxcitystock, price, goodincityid
		from `goodsincities` where `goodsincities`.`CityId` = cityid AND `goodsincities`.`GoodId` = goodid;
	SELECT `cargo`.`Amount` into userStock from `cargo` where `cargo`.`UserId` = userid and `cargo`.`GoodId` = goodid;
	if amount > userStock then
		return -1;
	end if;
	if citystock + amount > maxcitystock then
		return -2;
	end if;
	update `goodsincities` set `goodsincities`.`stock` = citystock + amount where `goodsincities`.`CityId` = cityid AND `goodsincities`.`GoodId` = goodid;
	update `users` set `users`.`CurrentGold` = `users`.`CurrentGold` + price, `users`.`HighestScore` = greatest(`users`.`HighestScore`, `users`.`CurrentGold`), `LastAction` = CURRENT_TIMESTAMP(6) where `id` = userid;
	if amount = userStock then
		delete from `cargo` where `cargo`.`userid` = userid and `cargo`.`goodid` = goodid;
	else
		update `cargo` set `cargo`.`amount` = `cargo`.`amount` - `amount` where `cargo`.`userid` = userid and `cargo`.`goodid` = goodid;
	end if;
	insert into `gamelog` (`UserId`, `goodincityId`, `Amount`, `Command`) values (userid, goodincityid, -amount, 'sell');
	return LAST_INSERT_ID();
  END //