<?php
	$funcName = "$command[0]Product";
	do
	{
		if (!isset ($command[1]) || !ctype_digit($command[1]))
		{
			echo "invalid good id\n";
			break;
		}
		
		$goodId = intval($command[1]);
		$query = "SELECT `ServerID` from `goods` where `id` = $goodId";
		if (!($res = $DB->query($query)))
			printDBError($query);
		if ($res->num_rows ==  0 || $res->fetch_row()[0] != $user->server)
		{
			echo "invalid good id\n";
			break;
		}	
		
		if (!isset ($command[2]) || !ctype_digit($command[2]) || intval($command[2]) <= 0)
		{
			echo "invalid amount\n";
			break;
		}
		$amount = intval($command[2]);
		$query = "SELECT $funcName($user->id, $goodId, $amount) as t";
		$res = $DB->query($query);
		if ($res)
		{
			$logid = $res->fetch_row()[0];
			if ($command[0] == 'buy')
			{
				switch($logid)
				{
					case -1: echo "insufficient capacity\n"; break;
					case -2: echo "insufficient goods\n"; break;
					case -3: echo "insufficient funds\n"; break;
					default: echo "done\n"; break; 
				}
				$cooldown = 0.5;
			}
			if ($command[0] == 'sell')
			{
				switch($logid)
				{
					case -1: echo "insufficient goods\n"; break;
					case -2: echo "insufficient town store\n"; break;
					default: echo "done\n"; break; 
				}
				$cooldown = 1;
			}
		}
		else
			printDBError($query);
	}while(false);