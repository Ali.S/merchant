<?php
	require_once(dirname(__FILE__).'/util/DB.php');
	$file = file_get_contents("php://input");
	$lang = $_GET['lang'];
	if (!in_array($lang, array('pas', 'cpp', 'py')))
	{
		echo "bad language\n";
		header('X-Upload-Status: Failed');
		die;
	}
	require_once(dirname(__FILE__)."/util/misc.php");
	require_once(dirname(__FILE__)."/user.php");
	$user = new User($_SESSION['uid'], false);
	$localFileId = mt_rand(10000,99999).$user->id.'-'.(new DateTime())->getTimeStamp().mt_rand(10000,99999);
	
	file_put_contents(dirname(__FILE__).'/../stash/'.$localFileId.'.zip', $file);
	$query = "INSERT INTO `submissions` (`Lang`, `UserId`, `FileId`) values ('$lang', $user->id, '$localFileId')";
	if (!$DB->query($query))
	{
		header('X-Upload-Status: failed');
		printDBError($query);
	}
	header('X-Upload-Status: succeed');
	echo "done\n";
	
	