<?php
	require_once(dirname(__FILE__)."/util/DB.php");
	require_once(dirname(__FILE__)."/util/misc.php");
	require_once(dirname(__FILE__)."/user.php");
	$user = new User($_SESSION['uid']);
	$rawCommand = file_get_contents("php://input");
	$command = explode(' ', $rawCommand);
	$cooldown = 0;
	switch($command[0])
	{
		case "query": require_once("query.php"); break;
		case "buy": 
		case "sell": require_once("trade.php"); break;
		case "move": require_once("move.php"); break;
		case "upgrade": require_once("upgrade.php"); break;
		case "reset": require_once("reset.php"); break;
		default:
			echo "bad command\n";
			break;
	}
	
	$query = "SELECT `TimeScale` FROM `servers` WHERE `Id` = $user->server";
	if (!($res = $DB->query($query)))
		printDBError($query);
	$cooldown = $cooldown / $res->fetch_row()[0];
	$user->setCooldown($cooldown);
	set_time_limit($cooldown * 10);
	usleep($cooldown * 1000000);
