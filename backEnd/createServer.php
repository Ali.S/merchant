<?php
	set_time_limit(1000);
	require_once(dirname(__FILE__)."/user.php");
	$user = new User($_SESSION['uid'], false);
	$user->setCooldown(0);
	if ($user->server != -1)
	{
		echo "You don't have sufficient privilages<br>";
		die;
	}
	
	if (!(
		isset($_REQUEST['name'])&&
		isset($_REQUEST['city'])&&
		isset($_REQUEST['good'])&&
		isset($_REQUEST['price'])&&
		isset($_REQUEST['cityPRange'])&&
		isset($_REQUEST['diff'])&&
		isset($_REQUEST['influence'])&&
		isset($_REQUEST['scale'])&&
		isset($_REQUEST['gold'])&&
		isset($_REQUEST['capacity'])&&
		isset($_REQUEST['inccap'])&&
		isset($_REQUEST['oracle'])&&
		isset($_REQUEST['inscity'])&&
		isset($_REQUEST['prod'])&&
		isset($_REQUEST['cons'])&&
		isset($_REQUEST['maxs'])
		))
	{
		echo file_get_contents(dirname(__FILE__).'/../FrontEnd/CreateServerForm.html');
		die;
	}
	require_once(dirname(__FILE__).'/util/DB.php');
	require_once(dirname(__FILE__).'/util/misc.php');
	
	$serverName = $DB->escape_string($_REQUEST['name']);
	$cityCount = intval($_REQUEST['city']);
	$goodCount = intval($_REQUEST['good']);
	$priceDiff = floatval($_REQUEST['diff']);
	$averagePrice = floatval($_REQUEST['price']);
	$cityPositionRange = floatval($_REQUEST['cityPRange']);
	$averageBuySellDifference = floatval($_REQUEST['diff']);
	$averageStockInfluenceOnPrices = floatval($_REQUEST['influence']) / 100;
	$timeScale = floatval($_REQUEST['scale']);
	$initialGold = floatval($_REQUEST['gold']);
	$initialCapacity = intval($_REQUEST['capacity']);
	$increaseCapacityCost = floatval($_REQUEST['inccap']);
	$oracleCost = floatval($_REQUEST['oracle']) / 100;
	$initialSpawnCity = intval($_REQUEST['inscity']);
	$averageProduction = floatval($_REQUEST['prod']);
	$averageCunsomption = floatval($_REQUEST['cons']);
	$averageMaxmimumStorage = floatval($_REQUEST['maxs']);
	$query = 
		"insert into `servers`".
			"(`name`, `TimeScale`, `InitialGold`, `InitialCapacity`, `IncreaseCapacityCost`, `OracleCost`, `InitialCity`) values ".
			"('$serverName', $timeScale, $initialGold, $initialCapacity, $increaseCapacityCost, $oracleCost, $initialSpawnCity)";
	if ($DB->query($query))
		$serverId = $DB->insert_id;
	else
		printDBError($query);
	
	function pushArrayIntoTable($table, &$arr)
	{
		global $DB;
		$keys = array_keys($arr[0]);
		$queryBase = "insert into `$table` (`".join("`,`", $keys)."`) values";
		foreach ($arr as $key => $value)
		{
			$query = $queryBase."('".join("','", $value)."')";
			if ($DB->query($query))
				$arr[$key]["id"] = $DB->insert_id;
			else			
				printDBError($query);
		}
	}
	
	$cities = [];
	for ($i =0; $i < $cityCount;$i++)
		$cities[] = array(
			"Name" => generateRandomString(mt_rand() % 5 + 5),
			"ServerId" => $serverId,
			"PosX" => (mt_rand() / mt_getrandmax() - 0.5) * 2 * $cityPositionRange,
			"PosY" => (mt_rand() / mt_getrandmax() - 0.5) * 2 * $cityPositionRange);
	$goods = [];
	for ($i = 0; $i < $goodCount; $i++)
		$goods[] = array(
			"Name" => generateRandomString(mt_rand() % 4 + 6),
			"ServerId" => $serverId);
	pushArrayIntoTable("cities", $cities);
	pushArrayIntoTable("goods", $goods);
	
	$goodCityRelation =[];
	foreach ($cities as $city)
		foreach ($goods as $good)
		{
			$tmp = array(
				"CityId" => $city["id"],
				"GoodId" => $good["id"],
				"BasePriceBuy" => gamma($averagePrice + $priceDiff),
				"BasePriceSell" => gamma($averagePrice - $priceDiff),
				"Production" => floor(gamma($averageProduction)),
				"Consumption" => ceil(gamma($averageCunsomption)),
				"Stock" => 0,
				"MaxStore" => floor(gamma($averageMaxmimumStorage)),
				"StockInfluenceOnprice" => gamma($averageStockInfluenceOnPrices));
			if ($tmp["BasePriceBuy"] < $tmp["BasePriceSell"])
			{
				$t=$tmp["BasePriceBuy"];
				$tmp["BasePriceBuy"]=$tmp["BasePriceSell"];
				$tmp["BasePriceSell"]=$t;
			}
			if ($tmp["BasePriceBuy"] - $tmp["BasePriceSell"] < $priceDiff / 2)
			{
				$tmp["BasePriceBuy"] += $priceDiff / 4;
				$tmp["BasePriceSell"] -= $priceDiff / 4;
			}
			$goodCityRelation[] = $tmp;
		}
	pushArrayIntoTable("goodsincities", $goodCityRelation);
	
?>
