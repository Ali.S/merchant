<?php
	session_start();
	
	if (!isset($_SESSION["uid"]))
		require_once(dirname(__FILE__)."/backEnd/login.php");
	if (!isset($_SESSION["uid"]))
	{
		require_once(dirname(__FILE__)."/frontEnd/loginForm.php");
		die;
	}
	if (isset($_GET["action"]))
	{
		$action = $_GET["action"];
		switch($action)
		{
		case "command":
			session_write_close();
			require_once(dirname(__FILE__)."/backEnd/command.php");
			break;
		case "create":
			session_write_close();
			require_once(dirname(__FILE__)."/backEnd/createServer.php");
			break;
		case "submit":
			session_write_close();
			require_once(dirname(__FILE__)."/backEnd/upload.php");
			break;
		case "logout":
			session_unset();
			session_destroy();
			setcookie(session_name(),'',0,'/');
			break;
		default:
			echo "bad action\n";
		}
	}
	else
		echo file_get_contents(dirname(__FILE__)."/frontEnd/gameViewer.html");
?>