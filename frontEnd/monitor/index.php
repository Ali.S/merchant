<html>
    <head>
        <meta charset="utf-8">
        <title>عنوان</title>
        <link rel="stylesheet" href="css/bootstrap.css"/>
        <link rel="stylesheet" href="css/bootstrap-rtl.css"/>
        <link rel="stylesheet" href="css/main.css"/>
    </head>
    <body>
        <img src="images/city_1.png" id="city" hidden="hidden">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9">
                    <div id="map">
                        <div class="shadow6 box"><h3>نقشه بازی</h3></div>
                        <div id="mapParent">
                            <canvas id="canvasMap" width="970" height="500">
                            </canvas>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3" id="rank">
                    <div class='tableHeader'>
                        <div style='width: 10%; float: right;'>رتبه</div>
                        <div style='width: 40%; float: right;'>نام</div>
                        <div style='width: 25%; float: right;'>دارایی</div>
                        <div style='width: 25%; float: right;'>امتیاز</div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/canvas.js"></script>
        <script src="js/rank.js"></script>
    </body>
</html>