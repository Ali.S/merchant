window.addEventListener("resize", onResize);

var canvas = document.getElementById("canvasMap");
var ctx = canvas.getContext("2d");
var cityImage = document.getElementById("city");
var citys=new Array(1);
ctx.font = "20em arial";
onResize();

function update(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var minx, maxx, miny, maxy;
    if(citys[1]){
        minx = maxx = parseFloat(citys[1].split(" ")[1]);
        miny = maxy = parseFloat(citys[1].split(" ")[2]);
    }else{
        console.log("the 'citys' array doesn`t have '1'");
    }
	console.debug(citys.length);
    for (var i = 2; i< citys.length-1;i++)
    {
        var city = citys[i].split(' ');
        minx = Math.min(minx, parseFloat(city[1]));
        maxx = Math.max(maxx, parseFloat(city[1]));
        miny = Math.min(miny, parseFloat(city[2]));
        maxy = Math.max(maxy, parseFloat(city[2]));
        maxx += 1;
        maxy += 1;
        minx -= 1;
        miny -= 1;
    }
        
        for (var i = 1; i< citys.length;i++)
        {
            var city = citys[i].split(' ');
            var x = (city[1] - minx) / (maxx - minx) * canvas.width;
            var y = (city[2] - miny) / (maxy - miny) * canvas.height;
            ctx.drawImage(cityImage,x,y,50,50);
            ctx.strokeText(city[0],x+10,y+10);
        }
}

setInterval(sendData,1000);

function onResize(){
	var style = window.getComputedStyle(canvas.parentElement, null)
	canvas.width = parseFloat(style.width);
	canvas.height = parseFloat(style.height);
    update();
}

function sendData()
{
    var query = "query towns";
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://localhost/?action=command");
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            citys = xhttp.responseText.split("\n");
            console.log(citys.length);
            if(citys.length > 1) update();
        }else{
            console.log("connection error");
        }
    };
    xhttp.send(query);
}